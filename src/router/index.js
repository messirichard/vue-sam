import Vue from 'vue';
import Router from 'vue-router';
import Login from '../components/page/Login/Login';
import Dashboard from '../components/page/Dashboard/Dashboard';
import Main from '../components/page/Dashboard/Main/Main';
import Api from '../components/page/Dashboard/Api/Api';
import Device from '../components/page/Dashboard/Device/Device';
import Widget from '../components/page/Dashboard/Widget/Widget';
import WidgetCollection from '../components/page/Dashboard/Widget/WidgetCollection';
import WidgetDefault from '../components/page/Dashboard/Widget/WidgetDefault';
import Users from '../components/page/Dashboard/Users/Users';
import Profile from '../components/page/Dashboard/Profile/Profile';
import Subcription from '../components/page/Dashboard/Subcription/Subcription';
import Config from '../components/page/Dashboard/Config/Config';

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Login',
            component: Login
        },
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: Dashboard,
            children:[
                {
                    path: 'main',
                    name: 'Main',
                    component: Main
                },
                {
                    path: 'api',
                    name: 'Api',
                    component: Api
                },
                {
                    path: 'device',
                    name: 'Device',
                    component: Device
                },
                {
                    path: 'widget',
                    name: 'Widget',
                    component: Widget
                },
                {
                    path: 'widgetcollect',
                    name: 'Widget Collection',
                    component: WidgetCollection
                },
                {
                    path: 'widgetdefault',
                    name: 'Widget Default',
                    component: WidgetDefault
                },
                {
                    path: 'users',
                    name: 'Users',
                    component: Users
                },
                {
                    path: 'profile',
                    name: 'Profile',
                    component: Profile
                },
                {
                    path: 'subcription',
                    name: 'Subcription',
                    component: Subcription
                },
                {
                    path: 'config',
                    name: 'Config',
                    component: Config
                }
            ]
        },
    ]
})